rm -rf ./public/annotations
mkdir -p ./public/annotations/{text,json}

#segment the annotation file
csplit -z ./annotations/annotations.conllu --prefix ./public/annotations/text/annotation_ --suffix "%05d.txt" /sent_id/ '{*}'

node ./modules/annotations-to-html/text-to-json.mjs
rm -rf ./public/annotations/text