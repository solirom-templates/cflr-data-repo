const MiniSearch = require("minisearch");
const fs = require("fs");

const replacements = {"ă": "a", "â": "a", "î": "i","ș": "s", "ş": "s","ț": "t","ţ": "t","á": "a","é": "e","í": "i","ó": "o","ú": "u","ắ": "a","ấ": "a","î́": "i"};
const replacementRegexp = new RegExp("[" + Object.keys(replacements).join("|") + "]", 'g')
const indexReplacements = {"-": "", "!": ""};
const indexReplacementRegexp = new RegExp("[" + Object.keys(indexReplacements).join("|") + "]", 'g');
const firstLetterReplacements = {"á": "a","é": "e","í": "i","ó": "o","ú": "u","ắ": "ă","ấ": "â","î́": "î"};
const firstLetterReplacementsRegexp = new RegExp("[" + Object.keys(firstLetterReplacements).join("|") + "]", 'g');

const documents = fs.readFileSync("./public/index/documents.txt")
    .toString()
    .split("\\")
    .filter(Boolean)
    .map(item => {
        const tokens = item.split("|"); 
        const firstLetter = tokens[2]
            .replace(firstLetterReplacementsRegexp, m => firstLetterReplacements[m]);
        
        return {"id": tokens[0], "s": tokens[1], "1l": firstLetter};
    });

const searcheableFields = ["s", "1l"];
const storedFields = ["s"];
const miniSearch = new MiniSearch({
    fields: searcheableFields,
    storeFields: storedFields
});
miniSearch.addAll(documents);  

const indexModule = {
    "index": miniSearch,
    "searcheableFields": searcheableFields,
    "storedFields": storedFields
};

fs.writeFileSync("./public/index/simple-search.json", JSON.stringify(indexModule));

const advancedSearch = [
  {
    "title": "Prima literă",
    "field": "1l",
    "buttons": [{"label":"A","title":"Cuvinte care încep cu litera A","value":"A"},{"label":"Ă","title":"Cuvinte care încep cu litera Ă","value":"Ă"},{"label":"B","title":"Cuvinte care încep cu litera B","value":"B"},{"label":"C","title":"Cuvinte care încep cu litera C","value":"C"},{"label":"D","title":"Cuvinte care încep cu litera D","value":"D"},{"label":"E","title":"Cuvinte care încep cu litera E","value":"E"},{"label":"F","title":"Cuvinte care încep cu litera F","value":"F"},{"label":"G","title":"Cuvinte care încep cu litera G","value":"G"},{"label":"H","title":"Cuvinte care încep cu litera H","value":"H"},{"label":"I","title":"Cuvinte care încep cu litera I","value":"I"},{"label":"Î","title":"Cuvinte care încep cu litera Î","value":"Î"},{"label":"J","title":"Cuvinte care încep cu litera J","value":"J"},{"label":"K","title":"Cuvinte care încep cu litera K","value":"K"},{"label":"L","title":"Cuvinte care încep cu litera L","value":"L"},{"label":"M","title":"Cuvinte care încep cu litera M","value":"M"},{"label":"N","title":"Cuvinte care încep cu litera N","value":"N"},{"label":"O","title":"Cuvinte care încep cu litera O","value":"O"},{"label":"P","title":"Cuvinte care încep cu litera P","value":"P"},{"label":"Q","title":"Cuvinte care încep cu litera Q","value":"Q"},{"label":"R","title":"Cuvinte care încep cu litera R","value":"R"},{"label":"S","title":"Cuvinte care încep cu litera S","value":"S"},{"label":"Ș","title":"Cuvinte care încep cu litera Ș","value":"Ș"},{"label":"T","title":"Cuvinte care încep cu litera T","value":"T"},{"label":"Ț","title":"Cuvinte care încep cu litera Ț","value":"Ț"},{"label":"U","title":"Cuvinte care încep cu litera U","value":"U"},{"label":"V","title":"Cuvinte care încep cu litera V","value":"V"},{"label":"W","title":"Cuvinte care încep cu litera W","value":"W"},{"label":"X","title":"Cuvinte care încep cu litera X","value":"X"},{"label":"Y","title":"Cuvinte care încep cu litera Y","value":"Y"},{"label":"Z","title":"Cuvinte care încep cu litera Z","value":"Z"}]
  }       
];

fs.writeFileSync("./public/index/advanced-search.json", JSON.stringify(advancedSearch));
