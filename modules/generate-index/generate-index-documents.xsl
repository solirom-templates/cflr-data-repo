<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" version="1.0">
    <xsl:output method="text" encoding="UTF-8" /> 
    
    <xsl:param name="uuid"/>
   
    <xsl:template match="/">
        <xsl:variable name="headword-form" select="/*//tei:form[@type = 'headword']/tei:orth" />
        <xsl:variable name="accentuated-form-1" select="normalize-space($headword-form)" />
        <xsl:variable name="homonym-number" select="normalize-space($headword-form/@n)" />
        <xsl:variable name="first-letter" select="substring($accentuated-form-1, 1, 1)" />
        <xsl:value-of select="concat($uuid, '|', $accentuated-form-1, $homonym-number, '|', $first-letter)" /><xsl:text>\\</xsl:text>        
    </xsl:template>     
</xsl:stylesheet>

<!--
xsltproc - -stringparam uuid "A1" modules/generate-index/generate-index-documents.xsl data/uuid-00173352-83c0-4318-9733-5283c053183c.xml
-->
