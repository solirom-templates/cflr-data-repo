import fsPromises from "fs/promises";
import fs from "fs";
import path from "path";
import os from "os";

const textAnnotationDir = path.resolve(".", "public/annotations/text");
const jsonAnnotationDir = path.resolve(".", "public/annotations/json");
const sentences = {};
const unorderedLexicon = {};
const poses = {
  "NOUN": "s.",
  "ADJ": "adj.",
  "CCONJ": "conj. coord.",
  "ADP": "adp.",
  "AUX": "aux.",
  "DET": "det.",
  "NUM": "num.",
  "PRON": "pron.",
  "PART": "part.",
  "VERB": "vb.",
  "ADV": "adv.",
  "SCONJ": "conj. subord.",
  "PROPN": "s. p.",
  "INTJ": "interj."
};

async function main(directory) {
  const fileNames = await fsPromises.readdir(directory);
  for (let file of fileNames) {
      const absolutePath = path.join(directory, file);
      
      const fileContent = await fs.promises.readFile(absolutePath, "utf8");
      const fileLines = fileContent
        .split(os.EOL)
        .filter(Boolean).filter(fileLine => !fileLine.includes("COMMA"))
        .filter(fileLine => !fileLine.includes("PERIOD"))
        .filter(fileLine => !fileLine.includes("HELLIP"))
        .filter(fileLine => !fileLine.includes("DBLQ"))
        .filter(fileLine => !fileLine.includes("SCOLON"))
        .filter(fileLine => !fileLine.includes("QUEST"))
        .filter(fileLine => !fileLine.includes("LPAR"))
        .filter(fileLine => !fileLine.includes("RPAR"))
        .filter(fileLine => !fileLine.includes("EXCL"))
        .filter(fileLine => !fileLine.includes("COLON"));
      const sentence_id = fileLines[0].split("=")[1].trim();
      const sentence_text = fileLines[1].split("=")[1].trim();
      sentences[sentence_id] = sentence_text;      

      const wordLines = fileLines.slice(3);
      wordLines.forEach(wordLine => {
        const wordMetadata = wordLine.split("\t");
        const headwordID = wordMetadata[0];
        const headword = wordMetadata[1];
        const lemma = wordMetadata[2];
        const pos = poses[wordMetadata[3]];
        const lexiconEntry = lemma + " " + pos;
        const lexiconEntryMetadata = {
          "sentenceID": sentence_id,
          "headwordID": headwordID,
          "headword": headword
        };

        if (lexiconEntry in unorderedLexicon) {
          unorderedLexicon[lexiconEntry].sentences.push(lexiconEntryMetadata);
        } else {
          unorderedLexicon[lexiconEntry] = {
            "pos": pos,  
            "sentences": [lexiconEntryMetadata]
          };
        }
      });
      
  }
}
// '1\tŞi\tși\tCCONJ\tCcssp\tPolarity=Pos\t5\tcc\t_\t_'
await main(textAnnotationDir);

const orderedLexicon = Object.keys(unorderedLexicon)
  .sort(new Intl.Collator("ro").compare)
  .reduce(
    (obj, key) => {
      obj[key] = unorderedLexicon[key];
      return obj;
    },
    {}
);

fs.writeFileSync(path.resolve(jsonAnnotationDir, "sentences.json"), JSON.stringify(sentences));
fs.writeFileSync(path.resolve(jsonAnnotationDir, "lexicon.json"), JSON.stringify(orderedLexicon));
